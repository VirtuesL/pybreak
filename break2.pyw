from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from time import strptime
from datetime import timedelta
import sys, os

def rp(relative_path):
  try:
      base_path = sys._MEIPASS
  except Exception:
       base_path = os.path.abspath(".")
  return os.path.join(base_path, relative_path)


class tray(QSystemTrayIcon):
  showSignal = pyqtSignal(int)
  def __init__(self, parent=None):
    QSystemTrayIcon.__init__(self, parent)

    self.icon = QIcon()
    self.icon.addFile(rp('serena.ico'))
    self.setIcon(self.icon)
    self.rMenu = QMenu(parent)
    showAction = QAction("Show", self)
    timeAction = QAction("Timer", self)
    quitAction = QAction("Quit", self)
    showAction.triggered.connect(lambda: self.showSignal.emit(0))
    timeAction.triggered.connect(lambda: self.showSignal.emit(1))
    quitAction.triggered.connect(qApp.quit)
    self.rMenu.addActions([showAction, timeAction, quitAction])
    self.setContextMenu(self.rMenu)
    self.activated.connect(lambda e: self.showSignal.emit(0) if e == 2 else "pass")

  def show(self):
    QSystemTrayIcon.show(self)

class LCD(QLCDNumber):
  def __init__(self, parent=None):
    super(LCD, self).__init__(parent)
    self.setSegmentStyle(QLCDNumber.Filled)
    self.setAttribute(Qt.WA_TranslucentBackground, True)
    self.setWindowFlag(Qt.FramelessWindowHint)
    self.setWindowFlag(Qt.WindowStaysOnTopHint)
    self.setWindowTitle("Remaining time")
    self.setWindowOpacity(0.70)

    self.timer = QTimer()
    self.setStyleSheet("background: transparent")
    self.resize(200, 60)

  def showTime(self, time):
    t = timedelta(milliseconds=time)
    self.display(str(t).split('.')[0])

  def closeEvent(self, event):
    self.hide()
    event.ignore()

  def mouseDoubleClickEvent(self, event):
    if bool(self.windowFlags() & Qt.FramelessWindowHint):
      self.setWindowFlags(self.windowFlags() & ~Qt.FramelessWindowHint)
    else:
      self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
    self.show()

class mainWindow(QWidget):
  submitSignal = pyqtSignal(dict)
  def __init__(self):
    super().__init__()
    self._iw()
  
  def _iw(self):
    self.setWindowTitle(u'PyBreak Timer')
    lGrid = QGridLayout(self)

    bBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    bBox.rejected.connect(self.hide)
    bBox.accepted.connect(self.submit)
    
    w = QLabel("Work time")
    b = QLabel("Break time")
    self.worktime = QLineEdit("01:00:00", self)
    self.breaktime = QLineEdit("00:05:00", self)
    self.strict = QCheckBox("Strict?", self)

    lGrid.addWidget(w, 0, 0, 1, 2)
    lGrid.addWidget(self.worktime, 1, 0, 1, 3)
    lGrid.addWidget(b, 2, 0, 1, 2)
    lGrid.addWidget(self.breaktime, 3, 0, 1, 3)
    lGrid.addWidget(self.strict, 4,0)
    lGrid.addWidget(bBox, 4, 2)

  def submit(self):
    work = strptime(self.worktime.text(), "%H:%M:%S")
    work = timedelta(hours=work.tm_hour, minutes=work.tm_min, seconds=work.tm_sec)
    pause = strptime(self.breaktime.text(), "%H:%M:%S")
    pause = timedelta(hours=pause.tm_hour, minutes=pause.tm_min, seconds=pause.tm_sec)
    self.submitSignal.emit({'strict':self.strict.isChecked(), 'work':work.total_seconds()*1000, 'pause': pause.total_seconds()*1000})
    self.hide()

  def closeEvent(self, event):
    reply = QMessageBox.question(self, 'Sure?', 'Are you sure you want to quit?', QMessageBox.Yes, QMessageBox.No)
    if reply == QMessageBox.Yes:
      event.accept()
    else:
      event.ignore()
  

class overlay(QWidget):
  breakSignal = pyqtSignal()
  def __init__(self, parent=None):
    super().__init__()
    self._iw()
    self.strict = False

  def _iw(self):
    self.setAttribute(Qt.WA_NoSystemBackground, True)
    self.setWindowFlag(Qt.FramelessWindowHint)
    self.setWindowFlag(Qt.WindowStaysOnTopHint)
    self.setWindowOpacity(0.70)
    
    hGrid = QGridLayout(self)
    self.label = QLabel("Time To Take A Break!")
    self.label.setFont(QFont("Arial", 50))
    self.label.setStyleSheet("color:#ffffff")
    timelabel = QLabel()
    timelabel.setStyleSheet("color:#ffffff")
    timelabel.setAlignment(Qt.AlignCenter)
    hGrid.addWidget(self.label, 1, 1)
    hGrid.addWidget(timelabel, 3, 1)

    hGrid.setColumnStretch(0,1)
    hGrid.setColumnStretch(2,1)
    hGrid.setRowStretch(2,1)
    hGrid.setRowStretch(0,1)

    self.setLayout(hGrid)
    self.showFullScreen()
    self.hide()

  def showEvent(self, event):
    self.setMouseTracking(False)
    event.accept()

  def mousePressEvent(self, event):
    if (not self.strict) | self.hasMouseTracking():
      self.breakSignal.emit()
      self.hide()
    else:
      print("Implement the label thing!")

  def setStrict(self, val):
    self.strict = val

  def closeEvent(self, event):
    event.ignore()
      
if __name__ == "__main__":
  app = QApplication(sys.argv)
  s=QWidget()
  t=tray(s)
  m=mainWindow()
  o=overlay()
  l=LCD()
  m.show()
  t.showSignal.connect(lambda s: (m.show(), m.raise_()) if s == 0 else l.show())

  workTimer = QTimer()
  workTimer.setSingleShot(True)
  pauseTimer = QTimer()
  pauseTimer.setSingleShot(True)
  o.breakSignal.connect(workTimer.start)
  workTimer.timeout.connect(lambda: (o.show(), pauseTimer.start()))
  pauseTimer.timeout.connect(lambda: o.setMouseTracking(True))

  m.submitSignal.connect(lambda dat: (workTimer.start(dat['work']), l.timer.start(1000), pauseTimer.setInterval(dat['pause']), o.setStrict(dat['strict'])))
  l.timer.timeout.connect(lambda: l.showTime(workTimer.remainingTime() & pauseTimer.remainingTime()))

  t.show()
  sys.exit(app.exec_())
