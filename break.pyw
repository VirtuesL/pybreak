from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from time import strptime
from datetime import timedelta
import sys


class tray(QSystemTrayIcon):
  def __init__(self, parent=None):
    QSystemTrayIcon.__init__(self, parent)
    self.main = mainWindow()
    self.overlay = overlay()
    self.main.timer.timeout.connect(self.overlay.show)
    self.overlay.breakSignal.connect(lambda: self.main.timer.start(self.main.workDuration))
    self.main.submitSignal.connect(lambda: self.overlay.startTimer(self.overlay.duration))

    self.icon = QIcon()
    self.icon.addFile('serena.ico')
    self.setIcon(self.icon)
    self.rMenu = QMenu(parent)
    showAction = QAction("Show", self)
    timeAction = QAction("Timer", self)
    quitAction = QAction("Quit", self)
    timeAction.triggered.connect(self.lcd.show)
    showAction.triggered.connect(self.main.show)
    quitAction.triggered.connect(qApp.quit)
    self.rMenu.addActions([showAction, timeAction, quitAction])
    self.setContextMenu(self.rMenu)
    self.activated.connect(self.sh)
    self.main.show()
  
  def sh(self, event):
    if event == QSystemTrayIcon.DoubleClick:
      self.main.show()

  def show(self):
    QSystemTrayIcon.show(self)

class LCD(QLCDNumber):
  def __init__(self, parent=None):
    super(LCD, self).__init__(parent)
    self.setSegmentStyle(QLCDNumber.Filled)

    self.timer = QTimer()

  def showTime(self, time):
    print(time)


class mainWindow(QWidget):
  submitSignal = pyqtSignal(dict)
  doneSignal = pyqtSignal()
  def __init__(self):
    super().__init__()
    self.timer = QTimer()
    self.timer.setSingleShot(True)
    self.workDuration = 3600000
    self._iw()
  
  def _iw(self):
    self.setWindowTitle(u'PyBreak Timer')
    lGrid = QGridLayout(self)

    bBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    bBox.rejected.connect(self.hide)
    bBox.accepted.connect(self.submit)
    
    w = QLabel("Work time")
    b = QLabel("Break time")
    self.worktime = QLineEdit("01:00:00", self)
    self.breaktime = QLineEdit("00:05:00", self)
    self.strict = QCheckBox("Strict?", self)

    lGrid.addWidget(w, 0, 0, 1, 2)
    lGrid.addWidget(self.worktime, 1, 0, 1, 3)
    lGrid.addWidget(b, 2, 0, 1, 2)
    lGrid.addWidget(self.breaktime, 3, 0, 1, 3)
    lGrid.addWidget(self.strict, 4,0)
    lGrid.addWidget(bBox, 4, 2)

  def submit(self):
    work = strptime(self.worktime.text(), "%H:%M:%S")
    work = timedelta(hours=work.tm_hour, minutes=work.tm_min, seconds=work.tm_sec)
    pause = strptime(self.breaktime.text(), "%H:%M:%S")
    pause = timedelta(hours=pause.tm_hour, minutes=pause.tm_min, seconds=pause.tm_sec)
    self.workDuration = work.total_seconds()*1000
    self.submitSignal.emit({'strict':self.strict.isChecked(),'duration': pause.total_seconds()*1000})
    self.timer.start(self.workDuration)
    self.hide()

  def closeEvent(self, event):
    reply = QMessageBox.question(self, 'Sure?', 'Are you sure you want to quit?', QMessageBox.Yes, QMessageBox.No)
    if reply == QMessageBox.Yes:
      event.accept()
    else:
      event.ignore()
  

class overlay(QWidget):
  breakSignal = pyqtSignal()
  def __init__(self, parent=None):
    super().__init__()
    self.timer = QTimer()
    self.timer.setSingleShot(True)
    self.timer.timeout.connect(lambda: self.setMouseTracking(True))
    self.strict = False
    self.duration = 5000
    self._iw()

  def _iw(self):
    self.setAttribute(Qt.WA_NoSystemBackground, True)
    self.setWindowFlag(Qt.FramelessWindowHint)
    self.setWindowOpacity(0.70)
    
    hGrid = QGridLayout(self)
    label = QLabel("Time To Take A Break!")
    label.setFont(QFont("Arial", 50))
    label.setStyleSheet("color:#ffffff")
    self.timelabel = QLabel()
    self.timelabel.setStyleSheet("color:#ffffff")
    self.timelabel.setAlignment(Qt.AlignCenter)
    hGrid.addWidget(label, 1, 1)
    hGrid.addWidget(self.timelabel, 3, 1)

    hGrid.setColumnStretch(0,1)
    hGrid.setColumnStretch(2,1)
    hGrid.setRowStretch(2,1)
    hGrid.setRowStretch(0,1)

    self.setLayout(hGrid)
    self.showFullScreen()
    self.hide()

  @pyqtSlot(dict)
  def setTimer(self, data):
    self.strict = data['strict']
    self.duration = data['duration']
    self.setMouseTracking(False)

  def showEvent(self, event):
    self.setMouseTracking(False)
    self.timer.start(self.duration)
    event.accept()

  def mousePressEvent(self, event):
    if (not self.strict) | self.hasMouseTracking():
      self.breakSignal.emit()
      self.hide()
    else:
      self.timelabel.setText("Come on! Wait at least "+str(timedelta(milliseconds=self.timer.remainingTime()))+"!")
      print("Come on! Wait at least", timedelta(milliseconds=self.timer.remainingTime()),"!")
      

if __name__ == "__main__":
  app = QApplication(sys.argv)
  s=QWidget()
  t=tray(s)
  t.show()
  sys.exit(app.exec_())
